# -*- coding: utf-8 -*-
"""
Created on Tue May 19 16:21:28 2020

@author: 13855
"""

import numpy as np
import pandas as pd

from mlxtend import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import GridSearchCV, train_test_split, RandomizedSearchCV
import matplotlib.pyplot as plt 
import seaborn as sns 
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.linear_model import Lasso

from sklearn.linear_model import SGDRegressor
from sklearn.model_selection import RandomizedSearchCV

from sklearn.ensemble import RandomForestRegressor
import pickle

from sklearn.externals import joblib
import pickle 
import warnings; warnings.simplefilter('ignore')


def add_columns(to_df, from_df):
    for col in from_df:
        if col not in to_df.columns:
            to_df[col] = 0
            
    return to_df


def get_col_type(df, col):
    vals = df[col].to_list()
    col_type = None
    for item in vals:
        if item is None:
            pass
        else:
            col_type = type(item)
            break
    return col_type
        
def drop_not_top(df, col, to_keep):
    """
    Function that deletes items from columns in dataframe that don't exist in the 'to_keep' list
    
    Parameters
    ----------
    df      (pd.DataFrame): Dataframe containing column col 
    col     (str)         : Column that you want to clean
    to_keep (list)        : List containing only values you want to keep in col
    
    Returns
    -------
    df (pd.DataFrame): Dataframe with col only containing values from to_keep 
    """
    col_type = get_col_type(df,col)

    total_skills_list = []
    
    if col_type == str:
        for candidate in df[col]:
            if candidate in to_keep:
                total_skills_list.append(candidate)
            else:
                total_skills_list.append("NaN")
                
    elif col_type == list:            
        for candidate in df[col]:
            if candidate is not None:
                total_skills_list.append([skill for skill in candidate if skill in to_keep])
            else:
                total_skills_list.append("NaN")
    else:
        raise ValueError('dtype of this column cannot be handled at this time')
                
    df['dropped_{}'.format(col)] = total_skills_list
    return df

def flatten(l):
    """
    Function that flattens a list of lists
    
    Parameters
    ----------
    l (list): List of lists to flatten
    
    Returns
    -------
    newl (list): Flattened list
    """
    # variable to store flattened list
    newl = []
    if l is not None:
        # Loop through every sublist
        for sublist in l:
            # Check for NoneTypes
            if sublist is not None:
                # Append every entry in sublist to single flat list
                for item in sublist:
                    newl.append(item)
                    
    return newl

def breakout_cols(df, col,num_skills):
    #Reformat the column
    
    previous_format = ar.format_data(df, [col])
    #Encode column into booelan matrix
    #Create transaction encoder
    te = preprocessing.TransactionEncoder()
    #Fit transaction encoder with previous jobs
    encoded_matrix = te.fit(previous_format).transform(previous_format, sparse=False) 
    #Encode te into matrix
    encoded_matrix = pd.DataFrame(encoded_matrix, columns=te.columns_)              

    #Now format matrix correctly
    
    #Ensure all values are numerical
    encoded_matrix = 1*encoded_matrix.fillna(0).transpose()
    #Get total numbers of occurences of each job
    total = encoded_matrix.sum(axis=1)   
    #Get total number of unique jobs present in the matrix
    total_previous = encoded_matrix.shape[0]
    #Add total jobs column
    encoded_matrix['total'] = total 
    
    #Trim down matrix to only jobs reaching a certain support
    support = encoded_matrix.total/encoded_matrix.shape[1]
    encoded_matrix['support'] = support
    encoded_matrix = encoded_matrix.sort_values(by='support', ascending=False)

    return encoded_matrix[:num_skills]


# =============================================================================
# 
# ###Import Data 
# 
# =============================================================================


#comp = pd.read_excel("../data/levels compensation - clean.xlsx", new=True, indicators=True, clean=True)
comp = pd.read_csv("../data/Ds_comps_&_sals.csv")
print(comp.shape)

#reading my titles data frame to try and normalize across
titles = pd.read_csv("../data/Normalized_Titles.csv")

#dropping unessary columns from titles that get imported in randomly

titles = titles.drop(columns = ['Unnamed: 4','Unnamed: 5', 'Unnamed: 6', 
                                ], axis = 1)


# =============================================================================
# 
# if necesary we could filter out all foreign observations, but will wait to do that, easiest way would be 
# to get out all locations that are list length >= 3, because those ones include the country names, so city, state, 
# country then convert back to string to one hot encode stuff, I think I'll go ahead and do this and delete if not 
# necessary 
# 
# =============================================================================



comp["location"] = comp.location.str.split(",")
us = comp[comp['location'].map(len) < 3]
us.reset_index(inplace = True)


#getting states for one hot encoding in future 
def get_states(df):
    states = []
    for i in range(len(df)):
        if len(df.location[i]) == 2:
            states.append(df.location[i][1][-2:])
        else:
            states.append("NULL") 
    df['states'] = states
get_states(us)

#turning back into string 
#going to dummies the top 10 and group else 
us["location"] = us['location'].apply(', '.join)

# #going to drop people with null base pay or people with base pay = '' because I found those and they wont
# #be helpful in a machine learning model 
# =============================================================================
# def replace_sal(df):
#     for i in range(len(df)):
#         if len(df['base compensation'][i]) == 0:
#             df['base compensation'][i] == 0
#         else:
#             pass
#     return df
# =============================================================================

#replace_sal(us)


#filling na's with '0', then gonna dorp them 
us.update(us[['base compensation','stock compensation',
              'bonus compensation']].fillna("$0.0"))
#us = us[us['base compensation'] != 0]


# =============================================================================
# us.reset_index(inplace = True)
# us.drop(columns = ['index'], axis = 1, inplace = True)
# =============================================================================

#need to change the total compensation numbers into ints
print(us.shape)
def clean_sal(df):
    df['base compensation'] = df['base compensation'].apply(lambda x: x[1:].replace(",",""))
    df['total compensation'] = df['total compensation'].apply(lambda x: x[1:].replace(",",""))

def get_ints(df):
    df['base compensation'] = df['base compensation'].apply(lambda x: float(x))
    df['total compensation'] = df['total compensation'].apply(lambda x: float(x))
    
clean_sal(us)
get_ints(us)



# =============================================================================
''' this is for plotting purposes and data visualization, if you want to see plots 
just un comment all of this '''

plt.scatter(x = us['base compensation'], y = us['total compensation'])
plt.title("relationship between base and total")
plt.xlabel("Base Compensation in USD")
plt.ylabel("Total Compensation in USD")
plt.show()
# =============================================================================
us = us[us['total compensation']<=350000]
#us = us[us['base compensation']<= 350000]

us.reset_index(inplace = True)
us.drop(columns = ['index'], axis = 1, inplace = True)


# =============================================================================
plt.scatter(x = us['base compensation'], y = us['total compensation'])
plt.title("Excluding Agregious outliers Relationship between base and total")
plt.xlabel("Base Compensation in USD")
plt.ylabel("Total Compensation in USD")
plt.show() 
plt.hist(us['total compensation'].tolist(),
        bins = 8, range = (0, 800000))
plt.title("Total Compensation Distribution")
plt.show()
'''Because of the skewness of the distribution, a way we can control for the 
heteroskedastic errors is to take the log of the outcome variables. This also 
allows us to interpret the coefficients as elasticities providing easier 
understanding and interpretation.'''
# =============================================================================

def get_logs(df):
    df['total compensation'] = df['total compensation'].apply(lambda x: np.log(x))
    df['base compensation'] = df['base compensation'].apply(lambda x: np.log(x))

get_logs(us)


us['base compensation'] = us['base compensation'].replace(-np.inf, np.median(us['base compensation']))
# =============================================================================
'''
now getting other feature will visualize 
''' 
# =============================================================================
t1 = pd.DataFrame(us['states'].value_counts().sort_values(ascending = False).head(12))
t1.reset_index(inplace = True)



def get_top_states(df):
    num_states = 3
    states_list = df.states.tolist()
    states_keep = list(pd.Series(states_list).value_counts()[:num_states].keys())
    states_keep
    
    dummies = pd.get_dummies(df.states)
    top = dummies[states_keep]
    state = top.sum(axis =1)
    state_other = []
    
    for i in range(len(state)):
        if state[i] ==1:
            state_other.append(0)
        else:
            state_other.append(1)
    top['state_other'] = state_other
    
    df1 = pd.concat([df, top], axis = 'columns')
    return df1

def get_top_tags(df):
    num_tags = 4
    tag_list = df.tag.tolist()
    tags_keep = list(pd.Series(tag_list).value_counts()[:num_tags].keys())
    tags_keep
    
    dummies = pd.get_dummies(df.tag)
    top = dummies[tags_keep] 
    
    df1 = pd.concat([df, top], axis = 'columns')
    return df1

def get_top_company(df):
    num_company = 3
    company_list = df.company.tolist()
    company_keep = list(pd.Series(company_list).value_counts()[:num_company].keys())
    
    dummies = pd.get_dummies(df.company)
    top = dummies[company_keep]
    state = top.sum(axis =1)
    state_other = []
    
    for i in range(len(state)):
        if state[i] ==1:
            state_other.append(0)
        else:
            state_other.append(1)
    top['company_other'] = state_other
    
    df1 = pd.concat([df, top], axis = 'columns')
    return df1
    


us = get_top_states(us)
#us = get_top_tags(us)
us = get_top_company(us)

#print(us.shape)

# =============================================================================
# 
# # first I need to make sure that all of the people i have who I want to standardize 
# #  work at companies with whom I have standaridized names 
# 
'''
Suggestion from Brad: instead of dropping make unknown the people who don't 
have any titles in the standerdized list 
'''

def get_normalized_titles(df1, df2):
    '''
    df1 is a data frame with the normalized columns for all companies 
    
    df2 is the data frame I have been wroking with, that has the data the salaries 
    locations, etc.


    
    df1.set_index(['Level'])
    #Creating a df where we have each title, corresponding to what each company calls them at each position
    
    df_2 = df1.melt(id_vars = ['Level'])
    df_3 = df_2.reset_index()
    df_3.drop(columns = ['index'], axis = 1, inplace = True)
    '''
    
    df_4 = pd.merge(df1, df2, how = 'inner', left_on = ['company', 'level name'], right_on = ['Company', 'Level'])
    #print(df_4.shape)
    return df_4
    
norm = get_normalized_titles(us, titles)

norm1 = norm.drop(columns = ['level_0', 'company', 'location', 
                             'Level', 'level name', 'stock compensation',
                             'bonus compensation', 'states', 'tag'
                             ,'hash_value', 'Job_title', "Level"], axis = 0)

def get_title_cols(df):
    dummies = pd.get_dummies(df['Normalized'])
    norm2 = pd.concat([df, dummies], axis = 'columns')
    norm2.drop(columns = ['Normalized', 'Company'], axis = 1, inplace = True)
    
    return norm2

norm2 = get_title_cols(norm1)
print("this is the eshape of the df ")
print(norm2.shape)

# =============================================================================
'''
Starting to model  
'''    
# =============================================================================


target = norm2['total compensation']
X = norm2.drop(columns = ['total compensation'], axis = 1)

#splitting into train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, target, test_size=0.20, 
                                                    random_state=17)

#going to train_test_val
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, 
                                                  test_size=0.2, 
                                                  random_state=17)

# Number of trees in random forest
n_estimators = [50, 100, 150, 250]

# Number of features to consider at every split
max_depth = [30, 40, 60, 75, 100]

max_depth.append(None)
min_samples_split = [2,4,6,8]

# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2]
# Method of selecting samples for training each tree

# Create the random grid
param_dictionary = {'n_estimators': n_estimators,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf}

'''
rf = RandomForestRegressor()
gs = GridSearchCV(rf, param_dictionary, cv = 2)
#go time 
#fitting the random forest 
gs.fit(X_train, y_train)

    
dictionary = dict(zip(X_train.columns, gs.best_estimator_.feature_importances_))
#print(dictionary)
y = np.sort(gs.best_estimator_.feature_importances_)[::-1]
x = X.columns[np.argsort(gs.best_estimator_.feature_importances_)[::-1]]
plt.barh(x[:10],y[:10])
plt.tight_layout()
plt.xlabel('Feature importance on testing')
plt.ylabel('Feature name')
plt.title('Feature importances in Prediction of Total Compensation by way of Random Forest on test set ')
plt.show() 

#predicting .....  
rf_pred = gs.predict(X_val)
rf_pred = np.e**rf_pred
mae_rf = mean_absolute_error(rf_pred, np.e**(y_val)) 
print(mae_rf)    

plt.scatter(x = rf_pred, y = np.e**y_val, color = "blue")
plt.title("Random Forest CV = 2, base salary included")
plt.xlabel('True Compensation')
plt.ylabel("Predicted Compensation")
plt.show()
'''  
    
# =============================================================================
'''
Want to try and see if I drop base salary, what will happen to my predictions and 
my error 
'''    
# =============================================================================
    

X2 = X.drop(columns = ['base compensation'], axis = 1)

    
#splitting into train_test_split
X_train2, X_test2, y_train, y_test = train_test_split(X2, target, test_size=0.20, 
                                                    random_state=17)

#going to train_test_val
X_train2, X_val2, y_train, y_val = train_test_split(X_train2, y_train, 
                                                  test_size=0.2, 
                                                  random_state=17)


rf2 = RandomForestRegressor()
gs2 = GridSearchCV(rf2, param_dictionary, cv = 2)
#go time 
#fitting the random forest 
gs2.fit(X_train2, y_train)


dictionary = dict(zip(X_train2.columns, gs2.best_estimator_.feature_importances_))
#print(dictionary)
y = np.sort(gs2.best_estimator_.feature_importances_)[::-1]
x = X.columns[np.argsort(gs2.best_estimator_.feature_importances_)[::-1]]
plt.barh(x[:10],y[:10])
plt.tight_layout()
plt.xlabel('Feature importance on testing')
plt.ylabel('Feature name')
plt.title('Feature importances in Prediction of Total Compensation by way of Random Forest on test set ')
plt.show() 

#predicting .....  
rf_pred2 = gs2.predict(X_val2)
rf_pred2 = np.e**rf_pred2
mae_rf = mean_absolute_error(rf_pred2, np.e**(y_val)) 
print(mae_rf)    

plt.scatter(x = rf_pred2, y = np.e**y_val, color = "blue")
plt.title("Random Forest CV = 2, base salary excluded")
plt.xlabel('True Compensation')
plt.ylabel("Predicted Compensation")
plt.show()

#joblib.dump(rf2, '../data/random_forest.sav')

#save trained model in pickle file
with open('final_forest_drop0.pkl', 'wb') as file:
    pickle.dump(gs2, file)


